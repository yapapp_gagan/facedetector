package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

//import com.google.android.gms.vision.Frame;
//import com.google.android.gms.vision.face.Face;
//import com.google.android.gms.vision.face.FaceDetector;

import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.FaceDetectors.OpencvDetector;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceData;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceDetectionImageParameters;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.DetectorListener;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.OnFaceSelectedListener;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.OnFacesDetectedListener;

/**
 * Created by gaganpreet on 5/8/17.
 */

public class FaceDetectionLayout extends RelativeLayout implements Facedetection_View.FaceDetectionViewListener {

    private RelativeLayout parent;
    private Facedetection_View facedetection_view;
    private OnFaceSelectedListener onFaceSelectedListener;
    private OnFacesDetectedListener onFacesDetectedListener;
//    private FaceDetector detector;
    private Bitmap image;
    private final String TAG_PARAM = "params";

    public FaceDetectionLayout(Context context) {
        super(context);
        onInit();
    }

    public FaceDetectionLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        onInit();
    }

    public FaceDetectionLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        onInit();
    }

    /**
     * This will save local variable of OnFacesDetectedListener.
     *
     * @param onFaceSelectedListener : object of OnFacesDetectedListener to get face face when user click on face.
     */
    public void setOnFaceSelectedListener(OnFaceSelectedListener onFaceSelectedListener) {
        this.onFaceSelectedListener = onFaceSelectedListener;
    }

    /**
     * Tasks on View initialised
     */
    private void onInit() {
        parent = new RelativeLayout(getContext());
        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        parent.setLayoutParams(layoutParams);
        facedetection_view = new Facedetection_View(getContext());
        ViewGroup.LayoutParams layoutParams_detector = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        facedetection_view.setLayoutParams(layoutParams_detector);
        parent.addView(facedetection_view);
        addView(parent);
    }


    /**
     * Saves image for detecting faces.
     *
     * @param image : image for detecting faces
     */
    public void setImage(Bitmap image) {
        this.image = image;
    }

    /**
     * This will start the detection process.
     * This should be called after setImage method
     */
//    public void startDetection() {
//        detector = new FaceDetector.Builder(getContext())
//                .setTrackingEnabled(false)
//                .setLandmarkType(FaceDetector.ALL_LANDMARKS)
//                .setMode(FaceDetector.FAST_MODE)
//                .build();
//        if (!detector.isOperational()) {
//            //Handle contingency
//            if (onFacesDetectedListener != null) {
//                onFacesDetectedListener.onDetectorNotOperational();
//            }
//        } else {
//            Frame frame = new Frame.Builder().setBitmap(image).build();
//
//            SparseArray<Face> faceSparseArray = detector.detect(frame);
//
//            if (faceSparseArray.size() > 0) {
//                Log.e("tag", " frames taken");
//                facedetection_view.setBitmap(image, this);
//
//                ArrayList<FaceDetectionImageParameters> fips = new ArrayList<>();
//
//                for (int i = 0; i < faceSparseArray.size(); i++) {
//
//
//                    FaceDetectionImageParameters faceDetectionImageParameters = new FaceDetectionImageParameters(
//                            faceSparseArray.get(i).getPosition().x
//                            , faceSparseArray.get(i).getPosition().y
//                            , (faceSparseArray.get(i).getPosition().x + faceSparseArray.get(i).getWidth())
//                            , (faceSparseArray.get(i).getPosition().y + faceSparseArray.get(i).getHeight())
//                    );
//
//                    fips.add(faceDetectionImageParameters);
//
//                }
//
//                facedetection_view.mFaces = fips;
//                Log.e("tag", " faces initialised");
//                facedetection_view.draw(new Canvas());
//            }
//
//
//            Log.e("tag", " invalidated");
//            if (detector.isOperational())
//                detector.release();
//
//            if (faceSparseArray.size() == 0) {
//                onFacesDetectedListener.noFaceDetected();
//                clear();
//            }
//
//        }
//
//
//    }
    public void startDetection() {

        OpencvDetector detector = new OpencvDetector(getContext());


        detector.detect(image, new DetectorListener() {
            @Override
            public void noFaceDetected() {

                if (onFacesDetectedListener != null) {
                    onFacesDetectedListener.noFaceDetected();
                }
                clear();
            }

            @Override
            public void onDetectorNotOperational() {
                if (onFacesDetectedListener != null) {
                    onFacesDetectedListener.onDetectorNotOperational();
                }
            }

            @Override
            public void onFacesDetected(ArrayList<FaceDetectionImageParameters> fips) {
                facedetection_view.setBitmap(image, FaceDetectionLayout.this);
                facedetection_view.mFaces = fips;
                Log.e("tag", " faces initialised");
                facedetection_view.draw(new Canvas());
            }
        });

//        if (!detector.isOperational()) {
//            //Handle contingency
//            if (onFacesDetectedListener != null) {
//                onFacesDetectedListener.onDetectorNotOperational();
//            }
//        } else
//
//        {
//            Frame frame = new Frame.Builder().setBitmap(image).build();
//
//            SparseArray<Face> faceSparseArray = detector.detect(frame);
//
//            if (faceSparseArray.size() > 0) {
//                Log.e("tag", " frames taken");
//                facedetection_view.setBitmap(image, this);
//
//                ArrayList<FaceDetectionImageParameters> fips = new ArrayList<>();
//
//                for (int i = 0; i < faceSparseArray.size(); i++) {
//
//
//                    FaceDetectionImageParameters faceDetectionImageParameters = new FaceDetectionImageParameters(
//                            faceSparseArray.get(i).getPosition().x
//                            , faceSparseArray.get(i).getPosition().y
//                            , (faceSparseArray.get(i).getPosition().x + faceSparseArray.get(i).getWidth())
//                            , (faceSparseArray.get(i).getPosition().y + faceSparseArray.get(i).getHeight())
//                    );
//
//                    fips.add(faceDetectionImageParameters);
//
//                }
//
//                facedetection_view.mFaces = fips;
//                Log.e("tag", " faces initialised");
//                facedetection_view.draw(new Canvas());
//            }
//
//
//            Log.e("tag", " invalidated");
//            if (detector.isOperational())
//                detector.release();
//
//            if (faceSparseArray.size() == 0) {
//                onFacesDetectedListener.noFaceDetected();
//                clear();
//            }
//
//        }


    }


    /**
     * This will save local variable of OnFacesDetectedListener.
     *
     * @param onFacesDetectedListener : object of onFacesDetectedListener to get all detected faces.
     */
    public void onFaceDetectedListener(OnFacesDetectedListener onFacesDetectedListener) {
        this.onFacesDetectedListener = onFacesDetectedListener;
    }


    @Override
    public void onFacesDrawn(ArrayList<FaceData> facesData) {

        if (onFacesDetectedListener != null) {
            onFacesDetectedListener.onFacesDetected(facesData);
        }

        switch (facesData.size()) {
            case 0:
                onFacesDetectedListener.noFaceDetected();
                clear();
                break;

/**
 * ToDo: Case 1 : code not removed so in future if we want to direct call onselect in case of 1 face.
 * */
//            case 1:
//                OnFacesDetectedListener.onFaceSelected(facesData.get(0).getImage());
//                break;


            default:
                for (int i = 0; i < facesData.size(); i++) {
                    FaceData faceData = facesData.get(i);
                    RelativeLayout relativeLayout = new RelativeLayout(getContext());
                    relativeLayout.setBackgroundColor(Color.parseColor("#22FF0000"));
                    relativeLayout.setOnClickListener(new OnFaceClickListener(faceData));

                    LayoutParams layoutParams = new LayoutParams((int)
                            (faceData.getParentParams().right - faceData.getParentParams().left)
                            , (int) (faceData.getParentParams().bottom - faceData.getParentParams().top));
                    layoutParams.topMargin = (int) faceData.getParentParams().top;
                    layoutParams.leftMargin = (int) faceData.getParentParams().left;

                    Log.e(TAG_PARAM, "width " + "rop: " + faceData.getParentParams().right
                            + " , lop: " + faceData.getParentParams().left);

                    Log.e(TAG_PARAM, "height " + "bop: " + faceData.getParentParams().bottom
                            + " , top: " + faceData.getParentParams().top);

                    Log.e(TAG_PARAM, "margins: " + ", top: " + layoutParams.topMargin + " , left: " + layoutParams.leftMargin);

                    addView(relativeLayout, layoutParams);
                }
                break;
        }
    }

    /**
     * clears all elements in this layout.
     * Reinitialise the customView
     */
    public void clear() {
        facedetection_view.clear();
        removeAllViews();
        onInit();
    }


    /**
     * This class inherit {@link OnClickListener} .
     * This will get {@link FaceData} in constructor.
     * When user clicks on the face. {@link OnFaceClickListener} will send Bitmap using onFaceSelectedListener object.
     */
    private class OnFaceClickListener implements OnClickListener {
        FaceData modalData;

        public OnFaceClickListener(FaceData modalData) {
            this.modalData = modalData;
        }

        @Override
        public void onClick(View view) {
            if (onFaceSelectedListener != null) {


                Log.e("onSelect_params_", (int) image.getWidth() + "," + (int) image.getHeight() + "," + (int) modalData.getBitmapParams().left
                        + "," + (int) modalData.getBitmapParams().top
                        + "," + (int) modalData.getBitmapParams().right
                        + "," + (int) modalData.getBitmapParams().bottom
                        + "," + (int) modalData.getBitmapParams().width
                        + "," + (int) modalData.getBitmapParams().height
                );


                Bitmap bitmap = Bitmap.createBitmap(image,
                        (int) modalData.getBitmapParams().left
                        , (int) modalData.getBitmapParams().top
                        , (int) modalData.getBitmapParams().width
                        , (int) modalData.getBitmapParams().height);

                Log.e("onSelect_params_", "cropped");

                onFaceSelectedListener.onFaceSelected(modalData, bitmap);

            }
        }
    }


}
