package cu.yapapp.com.facedetectiontutorial.camera;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cu.yapapp.com.facedetectiontutorial.R;
import cu.yapapp.com.facedetectiontutorial.customisableCamera.Connectivity;
import cu.yapapp.com.facedetectiontutorial.customisableCamera.CustomCameraManager;
import cu.yapapp.com.facedetectiontutorial.customisableCamera.CustomCameraView;

/**
 * Created by yapapp on 2/12/16.
 */
public class CameraView extends RelativeLayout
        implements View.OnClickListener,
        CustomCameraManager.FlashListener, CameraManager.CameratranslationInterface,
        View.OnLongClickListener {

    public ImageView imgChange_camera, imgCapture_image_button;
    public ImageView flash_icon, imgNoCameraAccess, imgSettings, imgVideoRecorder;
    public TextView auto_flash_icon, guide_txt, txtRecording/*,txtRecordingTimer*/;
    public LinearLayout lnrFlash, llPhotoGallery;
    public Chronometer chronometer;
    public RelativeLayout camera_tools_view;
    public CustomCameraManager customCameraManager;
    private LinearLayout frameCamera;
    Activity activity;
    private CameraViewInterface cameraVewInterface;
    public boolean allowVideoRecording;
    private boolean isChatFragment;
    private RelativeLayout relTimer;
    public long myElapsedMillis = 0;

    public CameraView(Context context) {
        super(context);
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void initialise(Activity context, CameraViewInterface cameraVewInterface){
        this.activity = context;
        this.cameraVewInterface = cameraVewInterface;
        init();
        setUpListeners();
    }

    private void setUpListeners() {
        frameCamera.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN
                        || motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                    cameraVewInterface.onTouch();
                }
                return false;
            }
        });
        imgCapture_image_button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {



                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        if (allowVideoRecording && isChatFragment) {
                            // Do something when the button is released.
                            stopVideoRecording();
                        }
                        break;
                }


                return false;
            }
        });
    }

    /**
     * Inflate your camera xml layout here
     */
    private void init() {
        inflate(getContext(), R.layout.view_camera_screen, this);
        llPhotoGallery = (LinearLayout) findViewById(R.id.llPhotoGalleryContainer);
        relTimer = (RelativeLayout) findViewById(R.id.relTimer);
        chronometer = (Chronometer) findViewById(R.id.chronometer);
        imgVideoRecorder = (ImageView) findViewById(R.id.imgVideoRecorder);
        imgChange_camera = (ImageView) findViewById(R.id.imgChange_camera);
        //txtRecordingTimer = (TextView) findViewById(R.id.txtRecordingTimer);
        flash_icon = (ImageView) findViewById(R.id.flash_icon);
        imgSettings = (ImageView) findViewById(R.id.imgSettings);
        imgCapture_image_button = (ImageView) findViewById(R.id.imgCapture_image_button);
        lnrFlash = (LinearLayout) findViewById(R.id.lnrFlash);
        imgNoCameraAccess = (ImageView) findViewById(R.id.imgNoCameraAccess);
        camera_tools_view = (RelativeLayout) findViewById(R.id.camera_tools_view);
        auto_flash_icon = (TextView) findViewById(R.id.auto_flash_icon);
        txtRecording = (TextView) findViewById(R.id.txtRecording);
        frameCamera = (LinearLayout) findViewById(R.id.frameCamera);
        guide_txt = (TextView) findViewById(R.id.guide_txt);

        customCameraManager = new CustomCameraManager(activity, frameCamera, cameraVewInterface, this);


        imgChange_camera.setOnClickListener(this);
        imgCapture_image_button.setOnClickListener(this);
        imgCapture_image_button.setOnLongClickListener(this);
        lnrFlash.setOnClickListener(this);
        llPhotoGallery.setOnClickListener(this);
        imgSettings.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llPhotoGalleryContainer:
                if (!Connectivity.isConnected(activity)) {
                    Toast.makeText(activity,
                            "No Internet Connection"
                            , Toast.LENGTH_SHORT).show();
                    return;
                }
                cameraVewInterface.onGalleryOptionClicked();
                break;
            case R.id.imgCapture_image_button:
                customCameraManager.onCameraOptionClicked();
                break;
            case R.id.imgChange_camera:
                if (CustomCameraView.isRecording)
                    Toast.makeText(activity, "Video Recording in Progress", Toast.LENGTH_SHORT).show();
                else
                    customCameraManager.onCameraFlip();
                break;
            case R.id.lnrFlash:
                if (CustomCameraView.isRecording)
                    Toast.makeText(activity, "Video Recording in Progress", Toast.LENGTH_SHORT).show();
                else {
                    if (isFlashCodeRunning) return;
                    isFlashCodeRunning = true;
                    setupFlashMode(customCameraManager.onCameraFlash());
                }
                break;
            case R.id.imgSettings:
                cameraVewInterface.onSettingClicked();
                break;
        }
    }

    public void takePicture() {
        imgCapture_image_button.performClick();
    }

    boolean isFlashCodeRunning = false;

    /**
     * Set text values for flash modes - on,off or auto
     *
     * @param mFlashMode
     */
    private void setupFlashMode(String mFlashMode) {
        try {
            if (Camera.Parameters.FLASH_MODE_AUTO.equalsIgnoreCase(mFlashMode)) {
                // cameraView.auto_flash_icon.setText("Auto");
                flash_icon.setImageResource(R.drawable.camera_auto_selector);
            } else if (Camera.Parameters.FLASH_MODE_ON.equalsIgnoreCase(mFlashMode)) {
                //cameraView.auto_flash_icon.setText("On");
                flash_icon.setImageResource(R.drawable.camera_flash_selector);
            } else if (Camera.Parameters.FLASH_MODE_OFF.equalsIgnoreCase(mFlashMode)) {
                // cameraView.auto_flash_icon.setText("Off");
                flash_icon.setImageResource(R.drawable.camera_flash_off_selector);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isFlashCodeRunning = false;

    }

    public void setFlashVisibility(int visibility) {
        lnrFlash.setVisibility(visibility);
    }

    public void stopCamera() {
        mCameraSwitchHandler.removeCallbacks(cameraSwitchRunnable);
        cameraSwitchRunnable = new CameraSwitchRunnable();
        toStart = false;
        mCameraSwitchHandler.postDelayed(cameraSwitchRunnable, time);
    }


    public void stopCameraImmediately() {
        mCameraSwitchHandler.removeCallbacks(cameraSwitchRunnable);
        cameraSwitchRunnable = new CameraSwitchRunnable();
        toStart = false;
//        mCameraSwitchHandler.postDelayed(cameraSwitchRunnable, time_immediate);
        if (!customCameraManager.isCameraReleased()) {
            customCameraManager.releaseCamera();
        }
    }

    public void startCamera() {
        mCameraSwitchHandler.removeCallbacks(cameraSwitchRunnable);
        cameraSwitchRunnable = new CameraSwitchRunnable();
        toStart = true;
        mCameraSwitchHandler.postDelayed(cameraSwitchRunnable, time_immediate);

    }

    //private final int time = 5000;
    private final int time = 0;
    //private final int time_immediate = 500;
    private final int time_immediate = 0;
    private Handler mCameraSwitchHandler = new Handler();

    CameraSwitchRunnable cameraSwitchRunnable = new CameraSwitchRunnable();
    private boolean toStart;

    public void removeTopMargin() {

    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.imgCapture_image_button:
                // Start video recording
                initiateVideoRecording();
                break;
        }
        return true;
    }

    /**
     * Long hold to record video
     */
    public void initiateVideoRecording() {
        if (isChatFragment) {
            allowVideoRecording = true;
            if (allowVideoRecording) {
                cameraVewInterface.initiateVideoRecording();
            }
        }
    }

    /**
     * Stop the ongoing video recording
     */
    public void stopVideoRecording() {
        allowVideoRecording = false;
        customCameraManager.stopVideoRecording();
    }

    public void setChatFragmentInstance(boolean isChatFragment) {
        this.isChatFragment = isChatFragment;
    }

    /**
     * Video recording is in progress
     */
    public void onVideoRecordingStarted() {
        myElapsedMillis = 0;
        txtRecording.setText(R.string.recording);
        imgVideoRecorder.setVisibility(View.VISIBLE);
        imgChange_camera.setVisibility(View.GONE);
        flash_icon.setVisibility(View.GONE);
        imgCapture_image_button.setVisibility(View.INVISIBLE);
        relTimer.setVisibility(View.VISIBLE);
        chronometer.setBase(SystemClock.elapsedRealtime()); // set base time for a chronometer
        chronometer.start(); // start a chronometer
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                myElapsedMillis = SystemClock.elapsedRealtime() - chronometer.getBase();
                Log.e("onChronometerTick", "" + myElapsedMillis);
            }
        });
    }

    /**
     * Video recording has been stopped
     */
    public void onVideoRecordingStopped() {
        txtRecording.setText(R.string.hold_button_for_video);
        imgVideoRecorder.setVisibility(View.GONE);
        imgChange_camera.setVisibility(View.VISIBLE);
        flash_icon.setVisibility(View.VISIBLE);
        imgCapture_image_button.setVisibility(View.VISIBLE);
        relTimer.setVisibility(View.GONE);
        chronometer.stop(); // stop a chronometer
    }

    class CameraSwitchRunnable implements Runnable {
        @Override
        public void run() {
            if (toStart) {
//                start
                if (!customCameraManager.isCameraReleased()) return;
                customCameraManager.initialiseCamera();
            } else {
//                 stop the camera
                if (!customCameraManager.isCameraReleased()) {
                    customCameraManager.releaseCamera();
                }

                cameraSwitchRunnable = null;
            }
        }
    }


    public void showNoPermissionElements() {
        imgSettings.setVisibility(View.VISIBLE);
        imgCapture_image_button.setVisibility(View.GONE);
        imgChange_camera.setVisibility(View.GONE);
    }

    public void showOnPermissionElements() {
        imgSettings.setVisibility(View.GONE);
        imgCapture_image_button.setVisibility(View.VISIBLE);
        imgChange_camera.setVisibility(View.VISIBLE);
    }

    @Override
    public void translateCameraUp() {
        camera_tools_view.animate()
                .translationY(0)
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });
    }

    @Override
    public void translateCameraDown() {
       /* camera_tools_view.animate()
                .translationY(getResources().getDimension(R.dimen.height_bottom_bar))
                .alpha(1.0f)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });*/
    }


    /**
     * Specify your interfaces to pass result to Camera Fragment
     */
    public interface CameraViewInterface extends   CustomCameraManager.CustomCameraManagerListener {
        void onGalleryOptionClicked();

        void onTouch();

        void onSettingClicked();

        void initiateVideoRecording();
    }
}
