package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceDetectionImageParameters;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceData;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.OnFaceSelectedListener;

/**
 * Created by gaganpreet on 13/6/17.
 */

/***
 * This class is used to get parameters
 *
 */
class Facedetection_View extends View {

    private Bitmap mBitmap;
    public ArrayList<FaceData> modalDatas = new ArrayList<>();
    public ArrayList<FaceDetectionImageParameters> mFaces;
    private Canvas canvas;
    private FaceDetectionViewListener fdv;


    public Facedetection_View(Context context) {
        super(context);
    }

    public Facedetection_View(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Facedetection_View(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * @param image : image to perform
     */
    public void setBitmap(Bitmap image, FaceDetectionViewListener fdv) {
        mBitmap = image;
        this.fdv = fdv;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.e("tag", "onDraw");
        super.onDraw(canvas);
        if ((mBitmap != null) && (mFaces != null)) {

            super.onDraw(canvas);
            canvas.drawColor(Color.WHITE);
            double scale = drawBitmap(canvas);

            if (scale > 0) {

                Log.e("tag", "drawBitmapDone");
                drawFaceBox(canvas, scale);
                this.canvas = canvas;
            }

/*           Todo: drawFaceLandmarks(canvas , scale);*/

        }

    }

    /**
     * This method will draw Bitmap on View.
     *
     * @param canvas
     * @return scaling factor
     */
    private double drawBitmap(Canvas canvas) {
        double viewWidth = canvas.getWidth();
        double viewHeight = canvas.getHeight();

        Log.e("drawBitmap", "canvas params :" + viewWidth + "," + viewHeight);

        double imageWidth = mBitmap.getWidth();
        double imageHeight = mBitmap.getHeight();

        Log.e("drawBitmap", "image params :" + imageWidth + "," + imageHeight);

        double scale = Math.min(viewWidth / imageWidth, viewHeight / imageHeight);

        Log.e("scale", String.valueOf(scale));

        Rect destBounds = new Rect(0, 0, (int) (imageWidth * scale), (int) (imageHeight * scale));

        Log.e("scaled: ", "imageParams*scale=  " + (imageWidth * scale) + "  , " + (imageHeight * scale));

        canvas.drawBitmap(mBitmap, null, destBounds, null);
        return scale;
    }


    /**
     * @param canvas object of canvas.
     * @param scale  scaling factor { image wrt the view }
     */
    private void drawFaceBox(Canvas canvas, double scale) {
        Paint paint = new Paint();
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);

        modalDatas = new ArrayList<>();


        for (int i = 0; i < mFaces.size(); i++) {


            float left = 0;
            float top = 0;
            float right = 0;
            float bottom = 0;

//            Face face = mFaces.valueAt(i);

            FaceDetectionImageParameters fip = mFaces.get(i);

            float left_ = (fip.left);
            float top_ = (fip.top);
            float right_ = (fip.right);
            float bottom_ = (fip.bottom);

            left = (float) (left_ * scale);
            top = (float) (top_ * scale);
            right = (float) (right_ * scale);
            bottom = (float) (bottom_ * scale);


            canvas.drawRect(left, top, right, bottom, paint);

            FaceData modalData = new FaceData();

            modalData.setBitmapParams(getParamsForFaceCenteredInImage(left_, top_, right_, bottom_));

            modalData.setParams(new FaceDetectionImageParameters(left, top, right, bottom));

            modalDatas.add(modalData);

        }

        if (fdv != null) {
            fdv.onFacesDrawn(modalDatas);
            fdv = null;
        }
    }


    /**
     * This view is used to calculate the crop regions after centering a face.
     * It will return a square image in which the selected face will be in center.
     * The inputs are face paramteres.
     *
     * @param left   : the left coordinate of face
     * @param top    :  the top coordinate of face
     * @param right  : the right coordinate of face
     * @param bottom : the bottom coordinate of face
     * @return : Object of {@link FaceDetectionImageParameters} which will contain calculated coordinates.
     */
    private FaceDetectionImageParameters getParamsForFaceCenteredInImage(float left, float top, float right, float bottom) {

        final int width_ = mBitmap.getWidth();
        final int height_ = mBitmap.getHeight();

        if (left < 0) left = 0;
        if (top < 0) top = 0;
        if (right > width_) {
            right = width_;
        }
        if (bottom > height_) {
            bottom = height_;
        }

        float diff_from_right = width_ - right;

        if (left < diff_from_right) {

            right = right + left;
            left = 0;

        } else {
            left = left - (width_ - right);
            right = width_;
        }


        float diff_from_bottom = height_ - bottom;

        if (top < diff_from_bottom) {
            bottom = bottom + top;
            top = 0;
        } else {
            top = top - (height_ - bottom);
            bottom = height_;
        }

        FaceDetectionImageParameters faceDetectionImageParameters = new FaceDetectionImageParameters(left, top, right, bottom);
        return faceDetectionImageParameters;
    }

    /**
     * Clears paramters so that face will not be drawn when @onDraw is called.
     */
    public void clear() {
        try {
            mFaces = null;
/*           ToDo: please check after removing comment on this todo.
if(canvas !=null)
 canvas.drawColor(Color.TRANSPARENT , PorterDuff.Mode.CLEAR);
 */
            mBitmap = null;
            fdv = null;
        } catch (Exception | Error e) {
            e.printStackTrace();
        }

    }


    public interface FaceDetectionViewListener {
        public void onFacesDrawn(ArrayList<FaceData> modalDatas);
    }

}