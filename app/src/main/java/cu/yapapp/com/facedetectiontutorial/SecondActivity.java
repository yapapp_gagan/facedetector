package cu.yapapp.com.facedetectiontutorial;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.ByteArrayOutputStream;

import static cu.yapapp.com.facedetectiontutorial.Util.thumb;
import static java.security.AccessController.getContext;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_second);

        final ImageView ivProfile = (ImageView) findViewById(R.id.root);

//        ivProfile.setimage(thumb);




//        ivProfile = new ImageView(this);
//        LinearLayout.LayoutParams params = null;
//        switch (this.getResources().getDisplayMetrics().densityDpi) {
//            case DisplayMetrics.DENSITY_LOW:
//                break;
//            case DisplayMetrics.DENSITY_MEDIUM:
//                break;
//            case DisplayMetrics.DENSITY_HIGH:
//                break;
//            case DisplayMetrics.DENSITY_XHIGH:
//                params = new LinearLayout.LayoutParams(80, 80);
//                break;
//            case DisplayMetrics.DENSITY_XXHIGH:
//                params = new LinearLayout.LayoutParams(110, 110);
//                break;
//            case DisplayMetrics.DENSITY_XXXHIGH:
//                params = new LinearLayout.LayoutParams(150, 150);
//                break;
//        }
//        if (params == null) {
//            params = new LinearLayout.LayoutParams(80, 80);
//        }
//
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        thumb.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArray = stream.toByteArray();
//
//        params.setMargins((int) getResources().getDimension(R.dimen.right_icon_padding), 0, 0, 0);
////        ivProfile.setScaleType(ImageView.ScaleType.CENTER_CROP | ImageView.ScaleType.FIT_XY);
//        ivProfile.setLayoutParams(params);
//        final ImageView finalIvProfile = ivProfile;

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Util.thumb.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();




        Glide.with(this)
                .load(byteArray )
                .asBitmap()
                .centerCrop()
                .into(new BitmapImageViewTarget((ImageView) ivProfile) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        try {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            ivProfile.setImageDrawable(circularBitmapDrawable);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


    }




}
