package cu.yapapp.com.facedetectiontutorial.customisableCamera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;


import java.io.IOException;
import java.util.List;

import cu.yapapp.com.facedetectiontutorial.R;

/**
 * Created by Gurvinder on 18/10/2015.
 * This class assumes the parent layout is RelativeLayout.LayoutParams.
 */

public class CustomCameraView extends SurfaceView implements SurfaceHolder.Callback {
    private static boolean DEBUGGING = true;
    private static final String LOG_TAG = "CameraPreviewSample";
    private static final String CAMERA_PARAM_ORIENTATION = "orientation";
    private static final String CAMERA_PARAM_LANDSCAPE = "landscape";
    private static final String CAMERA_PARAM_PORTRAIT = "portrait";
    protected Activity mActivity;
    private SurfaceHolder mHolder;
    protected Camera mCamera;
    protected List<Camera.Size> mPreviewSizeList;
    protected List<Camera.Size> mPictureSizeList;
    protected Camera.Size mPreviewSize;
    protected Camera.Size mPictureSize;
    private int mSurfaceChangedCallDepth = 0;
    private int mCameraId;
    private String mFlashMode;
    private MediaRecorder mediaRecorder;
    private CustomCameraViewInterface customCameraViewInterface;
    PreviewReadyCallback mPreviewReadyCallback = null;
    public static boolean isRecording;
    private String videoFilePath;

    public boolean isCameraReleased() {
        return mCamera==null;
    }


    public static enum LayoutMode {
        FitToParent, // Scale to the size that no side is larger than the parent
        NoBlank // Scale to the size that no side is smaller than the parent
    }

    ;

    public interface PreviewReadyCallback {
        void onPreviewReady();
    }
    public interface CustomCameraViewInterface{
        void onVideoRecorded(String videoFilePath);
        void onVideoRecordingStarted();
        void onVideoRecordingStopped();
    }

    /**
     * State flag: true when surface's layout size is set and surfaceChanged()
     * process has not been completed.
     */
    protected boolean mSurfaceConfiguring = false;

    public CustomCameraView(Activity activity, int cameraId, String mFlashMode, LayoutMode mode) {
        super(activity); // Always necessary
        mActivity = activity;
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.mCameraId = cameraId;
        this.mFlashMode = mFlashMode;

        try {
            if(mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT){
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
            }else{
                mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
            }
            Camera.Parameters cameraParams = mCamera.getParameters();
            mPreviewSizeList = cameraParams.getSupportedPreviewSizes();
            mPictureSizeList = cameraParams.getSupportedPictureSizes();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(mHolder);
        } catch (Exception e) {
            if (mCamera != null) {
                mCamera.release();
            }
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        mSurfaceChangedCallDepth++;
        doSurfaceChanged(width, height);
        mSurfaceChangedCallDepth--;
    }

    private void doSurfaceChanged(int width, int height) {


        try {
            mCamera.stopPreview();

            Camera.Parameters cameraParams = mCamera.getParameters();
            boolean portrait = isPortrait();

            // The code in this if-statement is prevented from executed again when surfaceChanged is
            // called again due to the change of the layout size in this if-statement.
            if (!mSurfaceConfiguring) {
                Camera.Size previewSize = determinePreviewSize(portrait, width, height);
                Camera.Size pictureSize = determinePictureSize(previewSize);
                if (DEBUGGING) {
                    Log.v(LOG_TAG, "Desired Preview Size - w: " + width + ", h: " + height);
                }
                mPreviewSize = previewSize;
                mPictureSize = pictureSize;
                //mSurfaceConfiguring = adjustSurfaceLayoutSize(previewSize, portrait, width, height);
                // Continue executing this method if this method is called recursively.
                // Recursive call of surfaceChanged is very special case, which is a path from
                // the catch clause at the end of this method.
                // The later part of this method should be executed as well in the recursive
                // invocation of this method, because the layout change made in this recursive
                // call will not trigger another invocation of this method.
                if (mSurfaceConfiguring && (mSurfaceChangedCallDepth <= 1)) {
                    return;
                }
            }

            configureCameraParameters(cameraParams, portrait);
            mSurfaceConfiguring = false;

            mCamera.startPreview();
        } catch (Exception e) {
            Log.w(LOG_TAG, "Failed to start preview: " + e.getMessage());

            // Remove failed size
            try {

            } catch (Exception e1) {
                mPreviewSizeList.remove(mPreviewSize);
                mPreviewSize = null;

                // Reconfigure
                if (mPreviewSizeList.size() > 0) { // prevent infinite loop
                    surfaceChanged(null, 0, width, height);
                } else {
                    Toast.makeText(mActivity, "Can't start preview", Toast.LENGTH_LONG).show();
                    Log.w(LOG_TAG, "Gave up starting preview");
                }
            }

        }

        if (null != mPreviewReadyCallback) {
            mPreviewReadyCallback.onPreviewReady();
        }
    }


    public Camera getCamera() {
        if (mCamera != null) {
            return mCamera;
        }
        return null;
    }


    /**
     * @param portrait
     * @param reqWidth  must be the value of the parameter passed in surfaceChanged
     * @param reqHeight must be the value of the parameter passed in surfaceChanged
     * @return Camera.Size object that is an element of the list returned from Camera.Parameters.getSupportedPreviewSizes.
     */
    protected Camera.Size determinePreviewSize(boolean portrait, int reqWidth, int reqHeight) {
        // Meaning of width and height is switched for preview when portrait,
        // while it is the same as user's view for surface and metrics.
        // That is, width must always be larger than height for setPreviewSize.
        int reqPreviewWidth; // requested width in terms of camera hardware
        int reqPreviewHeight; // requested height in terms of camera hardware
        if (portrait) {
            reqPreviewWidth = reqHeight;
            reqPreviewHeight = reqWidth;
        } else {
            reqPreviewWidth = reqWidth;
            reqPreviewHeight = reqHeight;
        }

        if (DEBUGGING) {
            Log.v(LOG_TAG, "Listing all supported preview sizes");
            for (Camera.Size size : mPreviewSizeList) {
                Log.v(LOG_TAG, "  w: " + size.width + ", h: " + size.height);
            }
            Log.v(LOG_TAG, "Listing all supported picture sizes");
            for (Camera.Size size : mPictureSizeList) {
                Log.v(LOG_TAG, "  w: " + size.width + ", h: " + size.height);
            }
        }

        // Adjust surface size with the closest aspect-ratio
        float reqRatio = ((float) reqPreviewWidth) / reqPreviewHeight;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        Camera.Size retSize = null;
        for (Camera.Size size : mPreviewSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }

        return retSize;
    }

    protected Camera.Size determinePictureSize(Camera.Size previewSize) {
        Camera.Size retSize = null;
        for (Camera.Size size : mPictureSizeList) {
            if (size.equals(previewSize)) {
                return size;
            }
        }

        if (DEBUGGING) {
            Log.v(LOG_TAG, "Same picture size not found.");
        }

        // if the preview size is not supported as a picture size
        float reqRatio = ((float) previewSize.width) / previewSize.height;
        float curRatio, deltaRatio;
        float deltaRatioMin = Float.MAX_VALUE;
        for (Camera.Size size : mPictureSizeList) {
            curRatio = ((float) size.width) / size.height;
            deltaRatio = Math.abs(reqRatio - curRatio);
            if (deltaRatio < deltaRatioMin) {
                deltaRatioMin = deltaRatio;
                retSize = size;
            }
        }

        return retSize;
    }


    public void takePicture(CustomCameraManager.CameraOrientationListener mOrientationListener, Camera.PictureCallback photoCallback) {
        if (mCamera != null) {

            mOrientationListener.rememberOrientation();
            mCamera.takePicture(new Camera.ShutterCallback() {
                @Override
                public void onShutter() {
                    Log.e("shutter" , "shutter");
                }
            }, null, null, photoCallback);// picture clicked here
        }
        else Log.e("error:" , "Camera is null");
    }

    /**
     * Record your video
     * @param mOrientationListener
     */
    public void startRecording(CustomCameraManager.CameraOrientationListener mOrientationListener,
                               final CustomCameraViewInterface customCameraViewInterface) {
        this.customCameraViewInterface = customCameraViewInterface;
        if (mCamera != null && !isRecording) {
            mOrientationListener.rememberOrientation();
            if (!prepareMediaRecorder(mOrientationListener)) {
                Toast.makeText(mActivity, "Error in recording video", Toast.LENGTH_SHORT).show();
                return;
            }
            isRecording = true;
            // work on UiThread for better performance
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Toast.makeText(mActivity, "Video Recording Started", Toast.LENGTH_SHORT).show();
                    mediaRecorder.start();
                    customCameraViewInterface.onVideoRecordingStarted();
                }
            });
        }
        else Log.e("error:" , "Camera is null or is already recording");
    }


    public void setFlash(String mFlashMode) {
        this.mFlashMode = mFlashMode;
        Camera.Parameters cameraParams = mCamera.getParameters();
        cameraParams.setFlashMode(mFlashMode);
        mCamera.setParameters(cameraParams);
    }


    @SuppressLint("NewApi")
    protected void configureCameraParameters(Camera.Parameters cameraParams, boolean portrait) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) { // for 2.1 and before
            if (portrait) {
                cameraParams.set(CAMERA_PARAM_ORIENTATION, CAMERA_PARAM_PORTRAIT);
            } else {
                cameraParams.set(CAMERA_PARAM_ORIENTATION, CAMERA_PARAM_LANDSCAPE);
            }
        } else { // for 2.2 and later
            int angle;
            Display display = mActivity.getWindowManager().getDefaultDisplay();
            switch (display.getRotation()) {
                case Surface.ROTATION_0: // This is display orientation
                    angle = 90; // This is camera orientation
                    break;
                case Surface.ROTATION_90:
                    angle = 0;
                    break;
                case Surface.ROTATION_180:
                    angle = 270;
                    break;
                case Surface.ROTATION_270:
                    angle = 180;
                    break;
                default:
                    angle = 90;
                    break;
            }
            Log.v(LOG_TAG, "angle: " + angle);
            mCamera.setDisplayOrientation(angle);
        }

        cameraParams.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        cameraParams.setPictureSize(mPictureSize.width, mPictureSize.height);

        if (cameraParams.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            cameraParams.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        }

        if (DEBUGGING) {
            Log.v(LOG_TAG, "Preview Actual Size - w: " + mPreviewSize.width + ", h: " + mPreviewSize.height);
            Log.v(LOG_TAG, "Picture Actual Size - w: " + mPictureSize.width + ", h: " + mPictureSize.height);
        }
        //  cameraParams.setRotation(90);
        if (mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
            cameraParams.setFlashMode(mFlashMode);
        }
        mCamera.setParameters(cameraParams);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stop();
    }

    public void stop() {
        if (null == mCamera) {
            return;
        }
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    public boolean isPortrait() {
        return (mActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    public void setOneShotPreviewCallback(PreviewCallback callback) {
        if (null == mCamera) {
            return;
        }
        mCamera.setOneShotPreviewCallback(callback);
    }

    public void setPreviewCallback(PreviewCallback callback) {
        if (null == mCamera) {
            return;
        }
        mCamera.setPreviewCallback(callback);
    }

    public Camera.Size getPreviewSize() {
        return mPreviewSize;
    }

    public void setOnPreviewReady(PreviewReadyCallback cb) {
        mPreviewReadyCallback = cb;
    }

    /**
     * Stop video recording
     */
    public void stopRecording() {
        if(isRecording) {
            isRecording = false;
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mediaRecorder.stop(); // stop the recording
                        releaseMediaRecorder(); // release the MediaRecorder object
                        //Toast.makeText(mActivity, "Video Saved!", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        releaseMediaRecorder();
                    }
                    customCameraViewInterface.onVideoRecordingStopped();
                    customCameraViewInterface.onVideoRecorded(videoFilePath);
                }
            });
        }
    }

    /**
     * Setup your media recorder
     * @param mOrientationListener
     * @return
     */
    private boolean prepareMediaRecorder(CustomCameraManager.CameraOrientationListener mOrientationListener) {
        int rotation = mOrientationListener.getRememberedNormalOrientation();
        mediaRecorder = new MediaRecorder();
        mCamera.unlock();
        mediaRecorder.setCamera(mCamera);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_720P));

        mediaRecorder.setMaxDuration(120000); // Set max duration 120 sec.
        // Create a video path in external directory
        videoFilePath = Utility.getInstance().createChatVideoDirectory().getPath();
        mediaRecorder.setOutputFile(videoFilePath);
        mediaRecorder.setMaxFileSize(10000000); // Set max file size 10MB
        // mediaRecorder.setVideoFrameRate(16); //might be auto-determined due to lighting
        mediaRecorder.setVideoEncodingBitRate(3000000);

        try {
            if(mCameraId == Camera.CameraInfo.CAMERA_FACING_FRONT){
                if(rotation==0) {
                    mediaRecorder.setOrientationHint(270);
                }
                else if(rotation==90)
                    mediaRecorder.setOrientationHint(180);

            }
            else if(mCameraId == Camera.CameraInfo.CAMERA_FACING_BACK ){
                //Log.e("Orientation",""+mOrientationListener.getRememberedNormalOrientation());
                if(rotation==0) {
                    mediaRecorder.setOrientationHint(90);
                }
                else if(rotation==90)
                    mediaRecorder.setOrientationHint(180);
            }
            mediaRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {

                public void onInfo(MediaRecorder mr, int fileSize, int extra) {
                    // When max file size reaches 10MB, stop recording
                    if(fileSize == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED){
                        stopRecording();
                    }
                }
            });

            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;
    }

    /**
     * Release media recorder when video recording is stopped
     */
    private void releaseMediaRecorder() {
        if (mediaRecorder != null) {
            isRecording = false;
            mediaRecorder.reset(); // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = null;
            mCamera.lock(); // lock camera for later use
        }
    }
}