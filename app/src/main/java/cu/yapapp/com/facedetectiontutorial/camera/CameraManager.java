package cu.yapapp.com.facedetectiontutorial.camera;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;

//import com.yapapp.cu.R;
//import com.yapapp.cu.model.enums.PhotoFetchConditions;
//import com.yapapp.cu.utils.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import cu.yapapp.com.facedetectiontutorial.R;
import cu.yapapp.com.facedetectiontutorial.customisableCamera.Utility;

/**
 * Created by gaganpreet on 27/2/17.
 */

public class CameraManager {

    private Activity context;
    private CameratranslationInterface cameratranslationInterface;
    private CameraManagerInterface cameraManagerInterface;
    private SettingsInterface settingsInterface;
    private String IMAGE_DIRECTORY_NAME = "Nehao Pictures";
    private File finalFile;
    public static final int REQUEST_CODE_ASK_PERMISSIONS_GALLERY = 1001;
    private Resources resources;
    private  boolean isCameraMode = false;
    private boolean alreadyScaled;


    public CameraManager(Activity context, CameratranslationInterface cameratranslationInterface
            , SettingsInterface settingsInterface
            , CameraManagerInterface cameraManagerInterface
    ) {
        this.context = context;
        resources = resources;
        this.cameratranslationInterface = cameratranslationInterface;
        this.cameraManagerInterface = cameraManagerInterface;
        this.settingsInterface = settingsInterface;

    }


    public void deleteFile() {
        if (finalFile != null && finalFile.exists()) {
            finalFile.delete();
        }
    }


    public void processGalleryImage(Intent data) {
        try {
            Uri selectedImageUri = data.getData();
            if (selectedImageUri == null) {
                cameraManagerInterface.showSnackAlert(resources.getString(R.string.unable_access_picture));
                return;
            }
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            finalFilePath = cursor.getString(columnIndex);
            cursor.close();
            if (TextUtils.isEmpty(finalFilePath)) {
                cameraManagerInterface.showSnackAlert(resources.getString(R.string.unable_access_picture));
                return;
            }

            File file = new File(finalFilePath);
            pictureValue = 1;
            performFileOperation(0,file);
            if(!TextUtils.isEmpty(finalFilePath))
                cameraManagerInterface.openCameraPreviewScreen(finalFilePath , PhotoFetchConditions.CU);
//            performCrop(finalFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            cameraManagerInterface.showSnackAlert(resources.getString(R.string.unable_access_picture));
        }
        catch (Error e){
            e.printStackTrace();
            cameraManagerInterface.showSnackAlert(resources.getString(R.string.unable_access_picture));
        }
    }

//    public void processPictureCropImage(Intent data){
//        boolean isErrorNeeded = false;
//        if(data==null){
//            isErrorNeeded = true;
//        }
//        if(!isErrorNeeded){
//            finalFilePath = data.getStringExtra(CropImage.IMAGE_PATH);
//
//            if (!TextUtils.isEmpty(finalFilePath)) {
//                cameraManagerInterface.openCameraPreviewScreen
//                        (finalFilePath, pictureValue == 0 ? PhotoFetchConditions.CU : PhotoFetchConditions.Gallery);
//            }
//            else isErrorNeeded = true;
//        }
//
//        if (isErrorNeeded){
//            cameraManagerInterface.showSnackAlert(resources.getString(R.string.error_in_fetching_image));
//        }
//
//    }

    public void onPictureClicked(byte[] data, int rotationValue ) {
        // Code to save Image goes here, save file and upload over server
        FileOutputStream outStream = null;
        try {
            File file =  Utility.getInstance().createFileInDirectory();
            finalFilePath = file.getAbsolutePath();
            outStream = new FileOutputStream(file);
            outStream.write(data);
            outStream.close();
            pictureValue = 0;
            performFileOperation(rotationValue,file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(outStream!=null){
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

//       performCrop(finalFilePath);
        cameraManagerInterface.openCameraPreviewScreen(finalFilePath , PhotoFetchConditions.CU);

    }

    public void setOnlyCameraMode() {
        isCameraMode = true;
    }


    public interface CameratranslationInterface {
        public void translateCameraUp();

        public void translateCameraDown();
    }

    public interface SettingsInterface {
        public void showSettingsMode();

        public void hideSettingsMode();
    }


    public interface CameraManagerInterface {
        public void openCameraPreviewScreen(String filePath, PhotoFetchConditions photoFetchConditions);

        public void showSnackAlert(String s);

//        public void openCroppingActivity(String file_path);
    }


    public enum PhotoFetchConditions {
        CU
    }


//    /**
//     * Crop image
//     * @param filePath
//     */
//    public void performCrop(String filePath) {
//        cameraManagerInterface.openCroppingActivity(filePath);
//    }



    private int pictureValue;
    private Bitmap mBitmap;
    private String finalFilePath;
    private String deviceName;
    private void performFileOperation(int rotationValue,File file) {
        FileInputStream fileInputStream = null;
        FileOutputStream outStream=null;
        try {
            alreadyScaled = false;
            String value=null;
            long Filesize= Utility.getInstance().getFolderSize(file)/1024;//call function and convert bytes into Kb
           /* if(Filesize>=1024)
                value=Filesize/1024+" Mb";
            else
                value=Filesize+" Kb";
            Toast.makeText(context,value,Toast.LENGTH_SHORT).show();*/
            fileInputStream = new FileInputStream(file);
            BitmapFactory.Options options = new BitmapFactory.Options();
            if(pictureValue == 0) {
                // Picture is from camera
                deviceName = android.os.Build.MODEL;
                if (!TextUtils.isEmpty(deviceName) && deviceName.contains("Moto G (4)")) {
                    alreadyScaled = true;
                    options.inSampleSize = 4; //Scale it down
                }
            }
          //  if(!TextUtils.isEmpty(deviceName) && deviceName.equalsIgnoreCase("SM-A500G")){
            if(!alreadyScaled && Filesize>1024)
            options.inSampleSize = 4; //Scale it down
         //   }
            options.inPreferredConfig = Bitmap.Config.RGB_565; // Less memory intensive color format
            mBitmap = BitmapFactory.decodeStream( fileInputStream, null, options );
            if(pictureValue == 1){
                // Picture is from gallery
                mBitmap = Utility.getInstance().rotateImageBy90Degree(finalFilePath,mBitmap);
                if(mBitmap == null)
                    return;
            }else{
                // Picture is from camera
                if (mBitmap == null)
                    return;
                if (rotationValue != 0) {
                    Bitmap oldBitmap = mBitmap;
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotationValue);
                    mBitmap = Bitmap.createBitmap(oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, true);
                    oldBitmap.recycle();
                }
            }
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 75, bao);
                byte[] mByteArray = bao.toByteArray();
                finalFile = Utility.getInstance().createFileInDirectory();
                outStream = new FileOutputStream(finalFile);
                outStream.write(mByteArray);
                finalFilePath = finalFile.getAbsolutePath();
                if(pictureValue == 0 && file.exists()){
                    file.delete();
                }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (Error e) {
            e.printStackTrace();
        }
        finally {
            if(fileInputStream!=null){
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }}

            if(outStream!=null){
                try {
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
