package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces;

import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceDetectionImageParameters;

/**
 * Created by gaganpreet on 9/8/17.
 */

public interface DetectorListener {
    public void noFaceDetected();

    public void onDetectorNotOperational();

    public void onFacesDetected(ArrayList<FaceDetectionImageParameters> fips);
}
