package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal;

/**
 * Created by gaganpreet on 5/8/17.
 */

/**
 * Class to save Paramters For Face wrt Face and Wrt screen
 */
public class FaceData {


    private FaceDetectionImageParameters bitmapParams;

    private FaceDetectionImageParameters parentParams;

    public void setBitmapParams(FaceDetectionImageParameters bitmapParams) {
        this.bitmapParams = bitmapParams;
    }


    public FaceDetectionImageParameters getBitmapParams() {
        return bitmapParams;
    }

    public void setParams(FaceDetectionImageParameters parentParams) {
        this.parentParams = parentParams;
    }


    public FaceDetectionImageParameters getParentParams() {
        return parentParams;
    }

    public void setParentParams(FaceDetectionImageParameters parentParams) {
        this.parentParams = parentParams;
    }
}
