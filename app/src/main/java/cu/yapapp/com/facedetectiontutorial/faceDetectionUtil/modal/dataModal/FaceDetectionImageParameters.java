package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal;

/**
 * Created by gaganpreet on 8/8/17.
 */

/**
 * Custom  Model class to store face paramters.
 */
public class FaceDetectionImageParameters {
    public float left, right, top, bottom, width, height;

    public FaceDetectionImageParameters(float left, float top, float right, float bottom) {
        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.top = top;
        this.width = right - left;
        this.height = bottom - top;
    }
}
