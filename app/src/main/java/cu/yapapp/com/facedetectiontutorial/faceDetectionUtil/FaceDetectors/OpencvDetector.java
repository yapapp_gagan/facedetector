package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.FaceDetectors;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.R;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceDetectionImageParameters;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.DetectorListener;

/**
 * Created by gaganpreet on 9/8/17.
 */

public class OpencvDetector extends FaceDetectors {


    public OpencvDetector(Context context) {
        super(context);
    }

    public void detect(Bitmap bitmap, DetectorListener onFacesDetectedListener) {

        if (!OpenCVLoader.initDebug()) {
            onFacesDetectedListener.onDetectorNotOperational();
        } else {
            ArrayList<FaceDetectionImageParameters> fips = new ArrayList<>();

            File libcascadeFrontalface = writeFrontalFace();

            if (libcascadeFrontalface == null) {
                throw new IllegalArgumentException("lbpcascade_frontalface.xml in res/raw not found");
            }


            CascadeClassifier cascadeClassifier
                    = new CascadeClassifier(libcascadeFrontalface.getAbsolutePath());
            Mat mat = new Mat();
            Utils.bitmapToMat(bitmap, mat);
            MatOfRect matOfRect = new MatOfRect();
            cascadeClassifier.detectMultiScale(mat, matOfRect);
            Rect[] rects = matOfRect.toArray();
            if (rects.length == 0) {
                onFacesDetectedListener.noFaceDetected();
            } else {
                for (int i = 0; i < rects.length; i++) {
                    Rect rect = rects[i];
                    FaceDetectionImageParameters fip = new FaceDetectionImageParameters(
                            rect.x, rect.y, rect.x + rect.width, rect.y + rect.height
                    );
                    fips.add(fip);
                }
                onFacesDetectedListener.onFacesDetected(fips);
            }
        }
    }

    private File writeFrontalFace() {

        InputStream inputStream = getContext().getResources().openRawResource(R.raw.lbpcascade_frontalface);
        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator
                + "opencv"
        );

        if (!file.exists())
            file.mkdirs();

        file = new File(file.getAbsolutePath() + File.separator + "lbpcascade_frontalface.xml");

        if (file.exists()) {
            return file;
        }

        FileOutputStream outputStream = null;


        try {
            file.createNewFile();

            outputStream = new FileOutputStream(file);

            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return file;
    }


}
