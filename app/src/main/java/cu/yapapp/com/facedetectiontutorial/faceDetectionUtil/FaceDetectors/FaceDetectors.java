package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.FaceDetectors;

import android.content.Context;
import android.graphics.Bitmap;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.DetectorListener;

/**
 * Created by gaganpreet on 9/8/17.
 */

public abstract class FaceDetectors {

    private Context context;

    FaceDetectors(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public abstract void detect(Bitmap bitmap, DetectorListener onFacesDetectedListener);

}
