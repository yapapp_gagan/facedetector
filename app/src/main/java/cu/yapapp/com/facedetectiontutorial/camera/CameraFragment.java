//package cu.yapapp.com.facedetectiontutorial.camera;
//
//import android.Manifest;
//import android.app.Fragment;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Handler;
//import android.provider.Settings;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Toast;
//
////import com.yaarchitectures.yacontroller.PagerYaFragment;
////import com.yaarchitectures.yacontroller.YABaseActivity;
////import com.yapapp.cu.R;
////import com.yapapp.cu.libraries.customisableCamera.CustomCameraManager;
////import com.yapapp.cu.model.enums.PhotoFetchConditions;
////import com.yapapp.cu.model.interfaces.CameraSwitch;
////import com.yapapp.cu.model.servicemanager.controllerservicemanager.CameraManager;
////import com.yapapp.cu.utils.Permissions;
////import com.yapapp.cu.utils.Permissions.PermissionInterface;
//
//import java.io.File;
//
//import cu.yapapp.com.facedetectiontutorial.R;
//import cu.yapapp.com.facedetectiontutorial.customisableCamera.CustomCameraManager;
//
///**
// * Created by yapapp on 2/12/16.
// */
//public class CameraFragment extends Fragment implements CameraView.CameraViewInterface,
//        CustomCameraManager.CustomCameraManagerInterface {
//
//    private CameraFragInterface cameraFragInterface;
//    public CameraView came2raView;
//    private final int GALLERY_IMAGE_REQUEST_CODE = 0;
//    private boolean onCreateCalled;
//    private CameraManager cameraManager;
//    // private boolean isOnlyCameraMode = false;
//    private boolean isSelected = false;
//    private boolean disAllowAppbar;
//    // private IncomingBroadcastReceiver broadcastReceiver = new IncomingBroadcastReceiver();
//    private boolean resultCancelled;
//    private Handler handler;
//    private boolean isVideoEnabled = true;
//
//    private boolean isFaceDetectionEnabled;
//
//    public void switchOn() {
//        isSelected = true;
//        if (cameraView != null)
//            startCamera();
//    }
//
//
//    public void switchOff() {
//        isSelected = false;
//        if (cameraView != null)
//            cameraView.stopCamera();
//    }
//
//
//    public void switchOffImmediately() {
//        isSelected = false;
//        taskSwitchOffImmediately();
//    }
//
//    private void taskSwitchOffImmediately() {
//        if (cameraView != null)
//            cameraView.stopCameraImmediately();
//    }
//
//    public void initialiseInterface(CameraFragInterface cameraFragInterface) {
//        this.cameraFragInterface = cameraFragInterface;
//        resultCancelled = false;
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//        if (cameraView == null) {
//            cameraView = new CameraView(getActivity(), this);
//
//            //registerIncomingBroadcastMessages();
//            handler = new Handler();
//            //if (isOnlyCameraMode)
//            if (disAllowAppbar) {
//                cameraView.translateCameraDown();
//                cameraView.guide_txt.setText(R.string.profile_text);
//            } else {
//                // cameraView.guide_txt.setText(getString(R.string.set_your_private_connection_picture));
//            }
//
//            cameraView.guide_txt.setVisibility(View.VISIBLE);
//            cameraView.llPhotoGallery.setVisibility(View.VISIBLE);
//            cameraView.txtRecording.setVisibility(View.GONE);
//            cameraView.setChatFragmentInstance(false);
//
//
//            onCreateCalled = true;
//            cameraManager = new CameraManager(getActivity(), cameraView, settingsInterface, cameraManagerInterface);
//
//            if (!isVideoEnabled) {
//                cameraView.guide_txt.setVisibility(View.GONE);
//                cameraView.llPhotoGallery.setVisibility(View.VISIBLE);
//                cameraView.txtRecording.setVisibility(View.GONE);
//                cameraView.setChatFragmentInstance(false);
//                disAllowAppbar = true;
//            }
//        }
//        return cameraView;
//    }
//
//
//    public boolean showAppBar() {
//        if (disAllowAppbar || resultCancelled)
//            return false;
//        else
//            return true;
//    }
//
//
//    /**
//     * Register your incoming broadcast messages
//     */
//  /*  private void registerIncomingBroadcastMessages() {
//        NehaoApplication.getApplication().registerReceiver(broadcastReceiver, new IntentFilter(getString(R.string.broadcastCaptureImage)));
//        NehaoApplication.getApplication().registerReceiver(broadcastReceiver, new IntentFilter(getString(R.string.broadcastRecordVideo)));
//    }*/
//
//
//    /**
//     * Allow to click picture using volume buttons
//     */
//    public void myOnKeyDown() {
//        cameraView.takePicture();
//    }
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        if (!isFirstRun) {
//            if (isSelected) {
//                startCamera();
//            }
//        } else {
//            isFirstRun = true;
//        }
//
//    }
//
//    boolean isFirstRun = false;
//
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        /*try {
//            if (broadcastReceiver != null) {
//                NehaoApplication.getApplication().unregisterReceiver(broadcastReceiver);
//            }
//        } catch (Exception e) {
//        }*/
//        switchOffImmediately();
//        if (cameraFragInterface != null)
//            cameraFragInterface.resetChatBottomView();
//
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        // Release your camera view
//        taskSwitchOffImmediately();
//    }
//
//    @Override
//    public void onGalleryOptionClicked() {
//
//
//        Permissions.getInstance(getContext()).checkReadExternalStoragePermissionRequired(new Permissions.PermissionInterface() {
//            @Override
//            public void requestForPermission() {
//                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
//                        Permissions.REQUEST_CODE_ASK_READ_EXTERNAL_STORAGE);
//            }
//
//            @Override
//            public void permisionAlreadyGranted() {
//                cameraView.stopCamera();
//                openGallery();
//            }
//        });
//
//    }
//
//    public void enableVideoRecording(boolean enableVideo) {
//        this.isVideoEnabled = enableVideo;
//    }
//
//    @Override
//    public void onTouch() {
//
//        if (disAllowAppbar)
//            return;
//        if (cameraFragInterface != null)
//            cameraFragInterface.onScreenTouch();
//    }
//
//    @Override
//    public void onSettingClicked() {
//        // Open app settings to enable app permissions
//        final Intent i = new Intent();
//        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//        i.addCategory(Intent.CATEGORY_DEFAULT);
//        i.setData(Uri.parse(getString(R.string.package_) + getActivity().getPackageName()));
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//        getActivity().startActivity(i);
//    }
//
//    @Override
//    public void initiateVideoRecording() {
//        Permissions.getInstance(getContext()).checkMultiplePermissionsVideoRecording(new Permissions.PermissionInterface() {
//            @Override
//            public void requestForPermission() {
//                requestPermissions(
//                        new String[]{Manifest.permission
//                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
//                        Permissions.REQUEST_CODE_ASK_MULTIPLE_PERMISSION_VIDEO_CALL);
//                //cameraView.allowVideoRecording = false;
//            }
//
//            @Override
//            public void permisionAlreadyGranted() {
//                // Permission Granted
//                cameraView.customCameraManager.startVideoRecording(CameraFragment.this);
//            }
//        });
//    }
//
//    boolean isSettingProvided = true;
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        try {
//            switch (requestCode) {
//                case Permissions.REQUEST_CODE_ASK_READ_EXTERNAL_STORAGE:
//                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                        // Permission Granted
//                        cameraView.stopCamera();
//                        openGallery();
//                    } else {
//                        //Permission denied
//                        Permissions.getInstance(getContext()).showPermissionSnackAlert(getActivity());
//                    }
//                    break;
//
//                case Permissions.REQUEST_CODE_ASK_PERMISSIONS_CAMERA: {
//                    if (grantResults.length > 0) {
//                        boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                        boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
//                        if (cameraPermission && readExternalFile) {
//                            if (isSelected) {
//                                // Permission Granted
//                                cameraView.showOnPermissionElements();
//                                if (onCreateCalled) {
//                                    onCreateCalled = false;
//                                    cameraView.startCamera();
//                                } else {
//                                    new Handler().postDelayed(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            cameraView.startCamera();
//                                        }
//                                    }, 500);
//                                }
//                            }
//                        } else {
//                            onCreateCalled = false;
//                            //   Permissions.getInstance(getContext()).showPermissionSnackAlert(getActivity());
//                            cameraView.showNoPermissionElements();
//                        }
//                    } else {
//                        onCreateCalled = false;
//                    }
//                }
//                break;
//                case Permissions.REQUEST_CODE_ASK_MULTIPLE_PERMISSION_VIDEO_CALL:
//                    if (grantResults.length > 0) {
//                        boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
//                        boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
//                        boolean audioPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
//                        if (cameraPermission && readExternalFile && audioPermission) {
//                            cameraView.allowVideoRecording = false;
//                        } else {
//                            cameraView.allowVideoRecording = false;
//                            Permissions.getInstance(getContext()).showPermissionSnackAlert(getActivity());
//                        }
//                    }
//                    break;
//                default:
//                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            }
//        } catch (ArrayIndexOutOfBoundsException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void startCamera() {
//
//        try {
//            Permissions.getInstance(getContext()).checkCameraPermissionsGranted(new Permissions.PermissionInterface() {
//                @Override
//                public void requestForPermission() {
//                    cameraView.imgNoCameraAccess.setVisibility(View.VISIBLE);
//                    if (onCreateCalled) {
//                        requestPermissions(
//                                new String[]{Manifest.permission
//                                        .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
//                                Permissions.REQUEST_CODE_ASK_PERMISSIONS_CAMERA);
//                    }
//                }
//
//                @Override
//                public void permisionAlreadyGranted() {
//                    // Permission Granted
//                    cameraView.imgNoCameraAccess.setVisibility(View.GONE);
//                    cameraView.showOnPermissionElements();
//
//                    cameraView.startCamera();
//                }
//            });
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
//
//
//    @Override
//    public void permissionsNotGranted() {
//        cameraView.showNoPermissionElements();
//    }
//
//
//    @Override
//    public void onPictureClicked(byte[] data, int photoRotation) {
//
//        cameraManager.onPictureClicked(data, photoRotation);
//    }
//
//
//    /**
//     * Get Home screen instance
//     */
//    public void setHomeInstance(CameraFragInterface cameraFragInterface) {
//        this.cameraFragInterface = cameraFragInterface;
//    }
//
//    /**
//     * Open android default gallery and select an image to share on server
//     */
//    private void openGallery() {
//        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            if (resultCode == getActivity().RESULT_OK) {
//                switch (requestCode) {
//                    case GALLERY_IMAGE_REQUEST_CODE:
//                        cameraManager.processGalleryImage(data);
//                        break;
////                    case Utility.PIC_CROP:
////                        cameraManager.processPictureCropImage(data);
////                        break;
//                }
//            } else {
//                resultCancelled = true;
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        showAppBar();
//                    }
//                }, 570);
//
//                startCamera();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//
//    CameraManager.SettingsInterface settingsInterface = new CameraManager.SettingsInterface() {
//        @Override
//        public void showSettingsMode() {
//            cameraView.showNoPermissionElements();
//        }
//
//        @Override
//        public void hideSettingsMode() {
//            cameraView.showOnPermissionElements();
//        }
//    };
//
//    CameraManager.CameraManagerInterface cameraManagerInterface = new CameraManager.CameraManagerInterface() {
//        @Override
//        public void openCameraPreviewScreen(final String capturedFilePath, final CameraManager.PhotoFetchConditions photoFetchConditions) {
//
//            CameraPreviewFragment cameraPreviewFragment = new CameraPreviewFragment();
//            cameraPreviewFragment.setFaceDetectionEnabled(isFaceDetectionEnabled);
//            cameraPreviewFragment.setCameraFragInterface(new CameraPreviewFragment.CameraPreviewInterface() {
//                @Override
//                public void performImageSaveTask(String filePath) {
//                    // startCamera();
//                    if (cameraFragInterface != null) {
//                        cameraFragInterface.onPictureObtained(capturedFilePath, photoFetchConditions.CU);
//                    }
//                }
//
//                @Override
//                public void performImageCancelTask() {
//                    startCamera();
//                    cameraManager.deleteFile();
//                }
//
//                @Override
//                public void enableCameraAgain() {
//
//                    startCamera();
//                    cameraManager.deleteFile();
//
//                }
//            });
//            cameraView.stopCamera();
//            cameraPreviewFragment.setImage(capturedFilePath);
//            replaceFragment(cameraPreviewFragment, true);
//
////            if (cameraFragInterface != null && photoFetchConditions != null)
////                cameraFragInterface.onPictureObtained(capturedFilePath, photoFetchConditions);
////            cameraView.stopCamera();
//        }
//
//        @Override
//        public void showSnackAlert(String message) {
//            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
//        }
//
//
//    };
//
//    public void setOnlyCameraMode() {
//        //  isOnlyCameraMode = true;
//    }
//
//    public boolean isSelected() {
//        return isSelected;
//    }
//
//    @Override
//    public void onVideoRecorded(String videoFilePath) {
//        if (cameraView.myElapsedMillis > 1) {
//            // popSingleFragment();
//            cameraFragInterface.onVideoRecorded(videoFilePath);
//        } else {
//            // Remove the unwanted file if file length is zero.
//            File file = new File(videoFilePath);
//            if (file.exists())
//                file.delete();
//            Toast.makeText(getActivity(), "No Video Recorded", Toast.LENGTH_LONG).show();
//        }
//    }
//
//    @Override
//    public void onVideoRecordingStarted() {
//        cameraView.onVideoRecordingStarted();
//    }
//
//    @Override
//    public void onVideoRecordingStopped() {
//        cameraView.onVideoRecordingStopped();
//    }
//
//    public void setFaceDetectionEnabled() {
//        isFaceDetectionEnabled = true;
//    }
//
//
//    /**
//     * Specify your interfaces to pass result to Home Fragment
//     */
//    public interface CameraFragInterface {
//        void onPictureObtained(String filePath, CameraManager.PhotoFetchConditions photoFetchConditions);
//
//        void onScreenTouch();
//
//        void onVideoRecorded(String videoFilePath);
//
//        void resetChatBottomView();
//    }
//
//
//}