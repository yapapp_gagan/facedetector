LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

#OpenCV
OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED

include /home/gaganpreet/Downloads/opencv_3.1.0_android_sdk/OpenCV-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_LDLIBS += -llog
LOCAL_MODULE := MyLibs
include $(BUILD_SHARED_LIBRARY)