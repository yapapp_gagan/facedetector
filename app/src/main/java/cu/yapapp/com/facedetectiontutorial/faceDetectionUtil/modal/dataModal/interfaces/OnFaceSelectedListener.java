package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces;

import android.graphics.Bitmap;

import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceData;

/**
 * Created by gaganpreet on 8/8/17.
 */

public interface OnFaceSelectedListener {

    public void onFaceSelected(FaceData faceData, Bitmap processedImage);

}
