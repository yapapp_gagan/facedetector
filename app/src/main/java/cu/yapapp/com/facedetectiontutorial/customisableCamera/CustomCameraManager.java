package cu.yapapp.com.facedetectiontutorial.customisableCamera;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by gaganpreet on 27/2/17.
 */

public class CustomCameraManager implements CustomCameraView.CustomCameraViewInterface {
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_START_CAMERA = 204;
    private Activity activity;
    private LinearLayout frameCamera;
    private String mFlashMode;
    private long mLastClickTime = 0;
    private long alottedTime = 1000;
    private CustomCameraManagerListener customCameraManagerListener;
    private FlashListener flashListener;
    private CustomCameraView cameraView;
    private int cameraId/* = Camera.CameraInfo.CAMERA_FACING_FRONT*/;
    private CameraOrientationListener mOrientationListener;
    private CustomCameraManagerInterface customCameraManagerInterface;

    public CustomCameraManager(Activity activity, LinearLayout frameForCamera, CustomCameraManagerListener customCameraManagerListener, FlashListener flashListener) {
        this.activity = activity;
        this.frameCamera = frameForCamera;
        mFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
        cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
        flashListener.setFlashVisibility(View.GONE);
        this.customCameraManagerListener = customCameraManagerListener;
        mOrientationListener = new CameraOrientationListener(activity);


        this.flashListener = flashListener;
    }

    /**
     * Click a single picture
     */
    public void onCameraOptionClicked() {
        clickPicture();
    }

    /**
     * Record a video
     */
    public void startVideoRecording(CustomCameraManagerInterface customCameraManagerInterface) {
        this.customCameraManagerInterface = customCameraManagerInterface;
        cameraView.startRecording(mOrientationListener, this);
    }

    /**
     * Stop ongoing video recoding
     */
    public void stopVideoRecording() {
        cameraView.stopRecording();
    }

    /**
     * Method to take your picture
     */
    private void clickPicture() {
        try {
            if (isMultiplePermissionsGranted(REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_START_CAMERA)) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < alottedTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                if (!Connectivity.isConnected(activity)) {
                    Toast.makeText(activity, "No Internet Connection", Toast.LENGTH_SHORT).show();
                    return;
                }
                cameraView.takePicture(mOrientationListener, photoCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Check for list of runtime permissions required
     *
     * @param type
     * @return - true/false for permission required or not
     */
    private boolean isMultiplePermissionsGranted(int type) {
        // Add permissions for camera and storage
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
        if (addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                customCameraManagerListener.permissionsNotGranted();
                return false;
            }
        }
        return true;
    }


    /**
     * Check for list of runtime permissions required
     *
     * @param type
     * @return - true/false for permission required or not
     */
    private boolean isVideoPermissionsGranted(int type) {
        // Add permissions for camera and storage
        List<String> permissionsNeeded = new ArrayList<>();
        final List<String> permissionsList = new ArrayList<>();
        if (addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE))
            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
        if (addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("CAMERA");
        if (addPermission(permissionsList, Manifest.permission.RECORD_AUDIO))
            permissionsNeeded.add("RECORD_AUDIO");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                customCameraManagerListener.permissionsNotGranted();
                return false;
            }
        }
        return true;
    }


    public void onCameraFlip() {
        try {
            if (isMultiplePermissionsGranted(REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_START_CAMERA)) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < alottedTime) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                cameraView.stop();
                if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                    flashListener.setFlashVisibility(View.VISIBLE);
                } else {
                    cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    flashListener.setFlashVisibility(View.GONE);
                }
                initialiseCamera();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String onCameraFlash() {
        try {
            if (isMultiplePermissionsGranted(REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS_START_CAMERA)) {
                mLastClickTime = SystemClock.elapsedRealtime();
                if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_AUTO)) {
                    mFlashMode = Camera.Parameters.FLASH_MODE_ON;
                } else if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_ON)) {
                    mFlashMode = Camera.Parameters.FLASH_MODE_OFF;
                } else if (mFlashMode.equalsIgnoreCase(Camera.Parameters.FLASH_MODE_OFF)) {
                    mFlashMode = Camera.Parameters.FLASH_MODE_AUTO;
                }

                cameraView.setFlash(mFlashMode);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mFlashMode;
    }


    /**
     * Setup your camera view
     */
    public void initialiseCamera() {
        try {
            cameraView = new CustomCameraView(activity, cameraId, mFlashMode, CustomCameraView.LayoutMode.FitToParent);
            LinearLayout.LayoutParams previewLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            frameCamera.addView(cameraView, 0, previewLayoutParams);

            for (int i = frameCamera.getChildCount() - 1; i > 0; i++) {
                frameCamera.removeViewAt(i);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mOrientationListener != null)
            mOrientationListener.enable();
    }


    /**
     * Check image rotation value
     *
     * @return
     */
    private int getPhotoRotation() {
        int rotation;
        int orientation = mOrientationListener.getRememberedNormalOrientation();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            rotation = (info.orientation - orientation + 360) % 360;
        } else {
            rotation = (info.orientation + orientation) % 360;
        }
        return rotation;
    }


    /**
     * Callback when picture gets clicked
     * byte[] returned
     */
    Camera.PictureCallback photoCallback = new Camera.PictureCallback() {
        public void onPictureTaken(final byte[] data, final Camera camera) {
            customCameraManagerListener.onPictureClicked(data, getPhotoRotation());

//
//            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inDither = false;
//            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//            Bitmap mBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
//            int rotationValue = getPhotoRotation();
//            try {
//                if (mBitmap != null) {
//                    if (rotationValue != 0) {
//                        Bitmap oldBitmap = mBitmap;
//                        Matrix matrix = new Matrix();
//                        matrix.postRotate(rotationValue);
//                        mBitmap = Bitmap.createBitmap(oldBitmap, 0, 0, oldBitmap.getWidth(), oldBitmap.getHeight(), matrix, true);
//                        oldBitmap.recycle();
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            customCameraManagerListener.onPictureClicked(mBitmap);
        }
    };


    /**
     * Check for storage and camera permissions for marshmallow
     *
     * @param permissionsList - storage amd camera
     * @param permission      - granted or revoked
     * @return
     */
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            return true;
        }
        return false;
    }

    public boolean isCameraReleased() {
        return cameraView == null || cameraView.isCameraReleased();
    }


    /**
     * Release your camera
     */
    public void releaseCamera() {
        try {
            if (cameraView != null)
                cameraView.stop();

            if (mOrientationListener != null)
                mOrientationListener.disable();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVideoRecorded(String videoFilePath) {
        customCameraManagerInterface.onVideoRecorded(videoFilePath);
    }

    @Override
    public void onVideoRecordingStarted() {
        customCameraManagerInterface.onVideoRecordingStarted();
    }

    @Override
    public void onVideoRecordingStopped() {
        customCameraManagerInterface.onVideoRecordingStopped();
    }


    public interface FlashListener {
        public void setFlashVisibility(int visibility);
    }

    public interface CustomCameraManagerInterface {
        void onVideoRecorded(String videoFilePath);

        void onVideoRecordingStarted();

        void onVideoRecordingStopped();
    }

    public interface CustomCameraManagerListener {
        void permissionsNotGranted();

        void onPictureClicked(byte[] data, int photoRotation);
    }


    /********************************camera orientation listener*******************************/

    /**
     * When orientation changes, onOrientationChanged(int) of the listener will be called
     */
    public class CameraOrientationListener extends OrientationEventListener {

        private int mCurrentNormalizedOrientation;
        private int mRememberedNormalOrientation;

        public CameraOrientationListener(Context context) {
            super(context, SensorManager.SENSOR_DELAY_NORMAL);
        }

        @Override
        public void onOrientationChanged(int orientation) {
            if (orientation != ORIENTATION_UNKNOWN) {
                mCurrentNormalizedOrientation = normalize(orientation);
            }
        }

        /**
         * @param degrees Amount of clockwise rotation from the device's natural position
         * @return Normalized degrees to just 0, 90, 180, 270
         */
        private int normalize(int degrees) {
            if (degrees > 315 || degrees <= 45) {
                return 0;
            }
            if (degrees > 45 && degrees <= 135) {
                return 90;
            }
            if (degrees > 135 && degrees <= 225) {
                return 180;
            }
            if (degrees > 225 && degrees <= 315) {
                return 270;
            }
            throw new RuntimeException("The physics as we know them are no more. Watch out for anomalies.");
        }

        public void rememberOrientation() {
            mRememberedNormalOrientation = mCurrentNormalizedOrientation;
        }

        public int getRememberedNormalOrientation() {
            rememberOrientation();
            return mRememberedNormalOrientation;
        }
    }

}
