package cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces;

import android.graphics.Bitmap;

import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceData;

/**
 * Created by gaganpreet on 9/8/17.
 */

    /**
     * It has methods which will send developer information if face is detected
     * , is Detector is operational and to get detected face.
     */
    public interface OnFacesDetectedListener {
        public void onFacesDetected(ArrayList<FaceData> bitmap);

        public void noFaceDetected();

        public void onDetectorNotOperational();

    }
