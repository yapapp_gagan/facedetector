package cu.yapapp.com.facedetectiontutorial.customisableCamera;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * Created by gaganpreet on 8/8/17.
 */

public class Utility {

    private static Utility utility = new Utility();

    public static Utility getInstance() {
        return utility;
    }

    public String DIRECTORY_NAME = "FaceDetection";
    public String VIDEO_DIRECTORY_NAME = "videos";


    /**
     * create directory for chat Video in sd card
     *
     * @return
     */
    public File createChatVideoDirectory() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_NAME), /*DIRECTORY_MEDIA + File.pathSeparator +*/ VIDEO_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        long timestamp = System.currentTimeMillis();
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID" + timestamp + ".mp4");
        return mediaFile;
    }



    public Bitmap rotateImageBy90Degree(String filePath, Bitmap bitmap) {

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(filePath);
            int exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotate = 0;
            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }
            if (rotate != 0) {
                int w = bitmap.getWidth();
                int h = bitmap.getHeight();
                // Setting pre rotate
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);
                // Rotating Bitmap & convert to ARGB_8888, required by tess
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
            }
            bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }



    /**
     * To check the size particular image present in file storage
     *
     * @param f
     * @return
     */
    public long getFolderSize(File f) {
        long size = 0;
        if (f.isDirectory()) {
            for (File file : f.listFiles()) {
                size += getFolderSize(file);
            }
        } else {
            size = f.length();
        }
        return size;
    }


    /**
     * Create a copy of the file selected fromm the gallery with timestamp appended
     */
    public File createFileInDirectory() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdirs();
        }
        // Create a media file name
        long timestamp = System.currentTimeMillis();
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG" + timestamp + ".jpg");
        return mediaFile;
    }


}
