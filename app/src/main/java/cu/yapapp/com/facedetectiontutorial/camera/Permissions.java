package cu.yapapp.com.facedetectiontutorial.camera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;

import cu.yapapp.com.facedetectiontutorial.R;

/**
 * Created by yapapp on 10/3/17.
 */
public class Permissions {
    public static final int REQUEST_CODE_ASK_READ_EXTERNAL_STORAGE = 300;
    public static final int REQUEST_CODE_ASK_PERMISSIONS_CAMERA = 301;
    public static final int REQUEST_CODE_ASK_PERMISSIONS_ONLY_CAMERA = 304;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSION_AUDIO_CALL = 302;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSION_VIDEO_CALL = 303;
    public static final int REQUEST_CODE_ASK_PERMISSION_ACCESS_FINE_LOCATION = 305;


    private static Permissions instance = null;
    PermissionInterface permissionInterface;
    public static Context context;

    public static Permissions getInstance(Context currentInstance) {
        if (instance == null) {
            context = currentInstance;
            instance = new Permissions();
            return instance;
        } else {
            return instance;
        }
    }

    /**
     * Check for camera group permission
     * <p>
     * /
     **/
    public void checkCameraPermissionsGranted(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasCameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(context,
                        Manifest.permission.CAMERA);
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }

    public void checkOnlyCameraGroupPermissionRequired(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasCameraPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }


    public void checkReadExternalStoragePermissionRequired(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }


    public void checkAccessFineLocationPermissionRequired(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }


    /**
     * Check for call multiple permission
     **/
    public void checkMultiplePermissionsAudioCallGranted(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasCallPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(context,
                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.RECORD_AUDIO) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_PHONE_STATE);
        if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }

    /**
     * Check for call multiple permission
     **/
    public void checkMultiplePermissionsVideoCallGranted(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasCallPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(context,
                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.RECORD_AUDIO) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_PHONE_STATE);

        if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }

    public void checkMultiplePermissionsVideoRecording(PermissionInterface permissionInterface) {
        this.permissionInterface = permissionInterface;
        int hasCallPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(context,
                        Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.RECORD_AUDIO);

        if (hasCallPermission != PackageManager.PERMISSION_GRANTED) {
            permissionInterface.requestForPermission();
        } else {
            permissionInterface.permisionAlreadyGranted();
        }
    }


    /**
     * Show snack alert if permission is denied with setting button on it
     *
     * @param activity
     */
    public AlertDialog showPermissionSnackAlert(final Activity activity) {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(activity);
        builder.setMessage("Access Denied");
        builder.setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.addCategory(Intent.CATEGORY_DEFAULT);
                intent.setData(Uri.parse("package:" + activity.getPackageName()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                activity.startActivity(intent);
            }
        });
        return builder.show();
    }
//
//
//    /**
//     * Show snack alert if permission is denied with setting button on it
//     * @param activity
//     */
//    public Snackbar getPermissionSnackAlert(final Activity activity){
//        Snackbar snackBar = Snackbar.make(((Activity) activity).coordinatorLayout, "Access denied!!", Snackbar.LENGTH_LONG).setAction("t", new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                final Intent i = new Intent();
//                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//                i.addCategory(Intent.CATEGORY_DEFAULT);
//                i.setData(Uri.parse("package:" + activity.getPackageName()));
//                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
//                activity.startActivity(i);
//            }
//        });
////        snackBar.show();
//        View snackbarView = snackBar.getView();
//        Button action = (Button) snackbarView.findViewById(android.support.design.R.id.snackbar_action);
//        action.setBackgroundResource(R.drawable.setting_selector);
//        action.setText("");
//        return snackBar;
//    }

    public interface PermissionInterface {
        void requestForPermission();

        void permisionAlreadyGranted();
    }

}
