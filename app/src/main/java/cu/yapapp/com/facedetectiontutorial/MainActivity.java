package cu.yapapp.com.facedetectiontutorial;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.util.ArrayList;

import cu.yapapp.com.facedetectiontutorial.camera.CameraManager;
import cu.yapapp.com.facedetectiontutorial.camera.CameraView;
import cu.yapapp.com.facedetectiontutorial.camera.Permissions;
import cu.yapapp.com.facedetectiontutorial.customisableCamera.CustomCameraManager;

import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.FaceData;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.OnFaceSelectedListener;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.modal.dataModal.interfaces.OnFacesDetectedListener;
import cu.yapapp.com.facedetectiontutorial.faceDetectionUtil.view.FaceDetectionLayout;

public class MainActivity extends AppCompatActivity implements
        OnFaceSelectedListener,
        OnFacesDetectedListener,
        CameraView.CameraViewInterface,
        CustomCameraManager.CustomCameraManagerInterface {

    private FaceDetectionLayout faceDetectionLayout;
    private Bitmap bitmap;
    private String TAG = "tag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraView = getCameraView();
        faceDetectionLayout = (FaceDetectionLayout) findViewById(R.id.faceDetectorLayout);
        faceDetectionLayout.setOnFaceSelectedListener(this);
        faceDetectionLayout.onFaceDetectedListener(this);

    }


    @Override
    public void noFaceDetected() {
        Toast.makeText(this, "No face detected", Toast.LENGTH_LONG).show();
        taskOnResetCamera();
    }

    private void taskOnResetCamera() {
        faceDetectionLayout.setVisibility(View.GONE);
        switchOffImmediately();
        startCamera();
        faceDetectionLayout.setVisibility(View.GONE);
    }

    @Override
    public void onDetectorNotOperational() {
        Toast.makeText(this, "Detector not working", Toast.LENGTH_LONG).show();
        taskOnResetCamera();
    }

    @Override
    public void onFacesDetected(ArrayList<FaceData> bitmap) {
        Toast.makeText(this, bitmap.size() + " faces detected", Toast.LENGTH_LONG).show();

        if (bitmap.size() > 0) {
            faceDetectionLayout.setVisibility(View.VISIBLE);
        } else {
            faceDetectionLayout.setVisibility(View.GONE);
        }

    }


    private CameraFragInterface cameraFragInterface;
    public cu.yapapp.com.facedetectiontutorial.camera.CameraView cameraView;
    private final int GALLERY_IMAGE_REQUEST_CODE = 0;
    private boolean onCreateCalled;
    private CameraManager cameraManager;
    // private boolean isOnlyCameraMode = false;
    private boolean isSelected = false;
    private boolean disAllowAppbar;
    // private IncomingBroadcastReceiver broadcastReceiver = new IncomingBroadcastReceiver();
    private boolean resultCancelled;
    private Handler handler;
    private boolean isVideoEnabled = true;

    private boolean isFaceDetectionEnabled;

    public void switchOn() {
        isSelected = true;
        if (cameraView != null)
            startCamera();
    }


    public void switchOff() {
        isSelected = false;
        if (cameraView != null)
            cameraView.stopCamera();
    }


    public void switchOffImmediately() {
        isSelected = false;
        taskSwitchOffImmediately();
    }

    private void taskSwitchOffImmediately() {
        if (cameraView != null)
            cameraView.stopCameraImmediately();
    }

    public void initialiseInterface(CameraFragInterface cameraFragInterface) {
        this.cameraFragInterface = cameraFragInterface;
        resultCancelled = false;
    }


    public cu.yapapp.com.facedetectiontutorial.camera.CameraView getCameraView() {

        if (cameraView == null) {
            cameraView = (CameraView) findViewById(R.id.cameraView);
            cameraView.initialise(this, this);

            //registerIncomingBroadcastMessages();
            handler = new Handler();
            //if (isOnlyCameraMode)
            if (disAllowAppbar) {
                cameraView.translateCameraDown();
                cameraView.guide_txt.setText(R.string.profile_text);
            } else {
                // cameraView.guide_txt.setText(getString(R.string.set_your_private_connection_picture));
            }

            cameraView.guide_txt.setVisibility(View.VISIBLE);
            cameraView.llPhotoGallery.setVisibility(View.VISIBLE);
            cameraView.txtRecording.setVisibility(View.GONE);
            cameraView.setChatFragmentInstance(false);


            onCreateCalled = true;
            cameraManager = new CameraManager(this, cameraView, settingsInterface, cameraManagerInterface);

            if (!isVideoEnabled) {
                cameraView.guide_txt.setVisibility(View.GONE);
                cameraView.llPhotoGallery.setVisibility(View.VISIBLE);
                cameraView.txtRecording.setVisibility(View.GONE);
                cameraView.setChatFragmentInstance(false);
                disAllowAppbar = true;
            }
        }

        return cameraView;
    }


    public boolean showAppBar() {
        if (disAllowAppbar || resultCancelled)
            return false;
        else
            return true;
    }


    /**
     * Allow to click picture using volume buttons
     */
    public void myOnKeyDown() {
        cameraView.takePicture();
    }


    @Override
    public void onResume() {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {

            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, baseLoaderCallback);

        } else {
            baseLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

        startCamera();

    }

    BaseLoaderCallback baseLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {

            switch (status) {
                case SUCCESS:
                    Toast.makeText(MainActivity.this, "success", Toast.LENGTH_LONG).show();
                    break;

                default:
                    super.onManagerConnected(status);
                    break;
            }

            super.onManagerConnected(status);
        }
    };


    @Override
    public void onPause() {
        super.onPause();
        // Release your camera view
        taskSwitchOffImmediately();
    }

    boolean isFirstRun = false;


    @Override
    public void onDestroy() {
        super.onDestroy();
        /*try {
            if (broadcastReceiver != null) {
                NehaoApplication.getApplication().unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
        }*/
        switchOffImmediately();
        if (cameraFragInterface != null)
            cameraFragInterface.resetChatBottomView();

    }

    static {
        System.loadLibrary("opencv_java3");
    }


    @Override
    public void onGalleryOptionClicked() {


        Permissions.getInstance(this).checkReadExternalStoragePermissionRequired(new Permissions.PermissionInterface() {
            @Override
            public void requestForPermission() {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        Permissions.REQUEST_CODE_ASK_READ_EXTERNAL_STORAGE);
            }

            @Override
            public void permisionAlreadyGranted() {
                cameraView.stopCamera();
                openGallery();
            }
        });

    }

    public void enableVideoRecording(boolean enableVideo) {
        this.isVideoEnabled = enableVideo;
    }

    @Override
    public void onTouch() {

        if (cameraFragInterface != null)
            cameraFragInterface.onScreenTouch();
    }

    @Override
    public void onSettingClicked() {
        // Open app settings to enable app permissions
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse(getString(R.string.package_) + getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(i);
    }

    @Override
    public void initiateVideoRecording() {
        Permissions.getInstance(this).checkMultiplePermissionsVideoRecording(new Permissions.PermissionInterface() {
            @Override
            public void requestForPermission() {
                requestPermissions(
                        new String[]{Manifest.permission
                                .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
                        Permissions.REQUEST_CODE_ASK_MULTIPLE_PERMISSION_VIDEO_CALL);
                //cameraView.allowVideoRecording = false;
            }

            @Override
            public void permisionAlreadyGranted() {
                // Permission Granted
                cameraView.customCameraManager.startVideoRecording(MainActivity.this);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        try {
            switch (requestCode) {
                case Permissions.REQUEST_CODE_ASK_READ_EXTERNAL_STORAGE:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        cameraView.stopCamera();
                        openGallery();
                    } else {
                        //Permission denied
                        Permissions.getInstance(this).showPermissionSnackAlert(this);
                    }
                    break;

                case Permissions.REQUEST_CODE_ASK_PERMISSIONS_CAMERA: {
                    if (grantResults.length > 0) {
                        boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (cameraPermission && readExternalFile) {
                            if (isSelected) {
                                // Permission Granted
                                cameraView.showOnPermissionElements();
                                if (onCreateCalled) {
                                    onCreateCalled = false;
                                    cameraView.startCamera();
                                } else {
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            cameraView.startCamera();
                                        }
                                    }, 500);
                                }
                            }
                        } else {
                            onCreateCalled = false;
                            //   Permissions.getInstance(this).showPermissionSnackAlert(MainActivity.this);
                            cameraView.showNoPermissionElements();
                        }
                    } else {
                        onCreateCalled = false;
                    }
                }
                break;
                case Permissions.REQUEST_CODE_ASK_MULTIPLE_PERMISSION_VIDEO_CALL:
                    if (grantResults.length > 0) {
                        boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean cameraPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        boolean audioPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                        if (cameraPermission && readExternalFile && audioPermission) {
                            cameraView.allowVideoRecording = false;
                        } else {
                            cameraView.allowVideoRecording = false;
                            Permissions.getInstance(this).showPermissionSnackAlert(MainActivity.this);
                        }
                    }
                    break;
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startCamera() {

        faceDetectionLayout.setVisibility(View.GONE);

        try {
            Permissions.getInstance(this).checkCameraPermissionsGranted(new Permissions.PermissionInterface() {
                @Override
                public void requestForPermission() {
                    cameraView.imgNoCameraAccess.setVisibility(View.VISIBLE);
                    if (onCreateCalled) {

                        try {
                            requestPermissions(
                                    new String[]{Manifest.permission
                                            .READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                    Permissions.REQUEST_CODE_ASK_PERMISSIONS_CAMERA);

                        } catch (Exception e) {
                            cameraView.startCamera();
                        } catch (Error e) {
                            cameraView.startCamera();
                        }

                    }


                }

                @Override
                public void permisionAlreadyGranted() {
                    // Permission Granted
                    cameraView.imgNoCameraAccess.setVisibility(View.GONE);
                    cameraView.showOnPermissionElements();

                    cameraView.startCamera();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void permissionsNotGranted() {
        cameraView.showNoPermissionElements();
    }


    @Override
    public void onPictureClicked(byte[] data, int photoRotation) {

        cameraManager.onPictureClicked(data, photoRotation);
    }


    /**
     * Get Home screen instance
     */
    public void setHomeInstance(CameraFragInterface cameraFragInterface) {
        this.cameraFragInterface = cameraFragInterface;
    }

    /**
     * Open android default gallery and select an image to share on server
     */
    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_IMAGE_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == MainActivity.this.RESULT_OK) {
                switch (requestCode) {
                    case GALLERY_IMAGE_REQUEST_CODE:
                        cameraManager.processGalleryImage(data);
                        break;
//                    case Utility.PIC_CROP:
//                        cameraManager.processPictureCropImage(data);
//                        break;
                }
            } else {
                resultCancelled = true;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showAppBar();
                    }
                }, 570);

                startCamera();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    CameraManager.SettingsInterface settingsInterface = new CameraManager.SettingsInterface() {
        @Override
        public void showSettingsMode() {
            cameraView.showNoPermissionElements();
        }

        @Override
        public void hideSettingsMode() {
            cameraView.showOnPermissionElements();
        }
    };

    CameraManager.CameraManagerInterface cameraManagerInterface = new CameraManager.CameraManagerInterface() {
        @Override
        public void openCameraPreviewScreen(final String capturedFilePath, final CameraManager.PhotoFetchConditions photoFetchConditions) {
            faceDetectionLayout.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(capturedFilePath);
            faceDetectionLayout.setImage(bitmap);
            faceDetectionLayout.startDetection();
        }

        @Override
        public void showSnackAlert(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        }


    };


    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void onVideoRecorded(String videoFilePath) {
        if (cameraView.myElapsedMillis > 1) {
            // popSingleFragment();
            cameraFragInterface.onVideoRecorded(videoFilePath);
        } else {
            // Remove the unwanted file if file length is zero.
            File file = new File(videoFilePath);
            if (file.exists())
                file.delete();
            Toast.makeText(MainActivity.this, "No Video Recorded", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onVideoRecordingStarted() {
        cameraView.onVideoRecordingStarted();
    }

    @Override
    public void onVideoRecordingStopped() {
        cameraView.onVideoRecordingStopped();
    }

    public void setFaceDetectionEnabled() {
        isFaceDetectionEnabled = true;
    }

    @Override
    public void onFaceSelected(FaceData faceData, Bitmap processedImage) {
        Util.thumb = processedImage;
        faceDetectionLayout.clear();
        startActivity(new Intent(this, SecondActivity.class));
    }


    /**
     * Specify your interfaces to pass result to Home Fragment
     */
    public interface CameraFragInterface {
        void onPictureObtained(String filePath, CameraManager.PhotoFetchConditions photoFetchConditions);

        void onScreenTouch();

        void onVideoRecorded(String videoFilePath);

        void resetChatBottomView();
    }


}
